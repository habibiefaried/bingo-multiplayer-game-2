#include <iostream>
#include <cstring>
#include "ClientSocket.h"
#include "SocketException.h"
#include "ServerSocket.h"
#include "Konversi.h"
#include "game.h"

int RangeNilai;
int LebarBoard;

bool IsDone = false;
int TebakanSaya;
int TebakanLawan;

int main()
{
	Bingo B;
	string IPTracker, IPPrev, IPNext, my_IP;
	string PortPrev, PortNext;
	int JmlOrang;
	
	int No,col,row;
	No = 0;
	string Name;
	string reply,status;
	string Input,data;
	
	int SPort = 5030;
	bool isGiliran = true;
	bool IsValid = false;
	bool isBoardOK = false;
	
	ClearScreen();
	cout<<"Masukkan Nama anda : "; cin>>Name;
	cout<<"Main dengan berapa orang : "; cin>>JmlOrang;
	cout<<"Masukkan IP Address Tracker : "; cin>>IPTracker;
	cout<<"========================="<<endl;
	cout<<"Aturan Board "<<endl;
	cout<<"========================="<<endl;
	
	/* Rangenilai + Lebar Board */
	while (!IsValid)
	{
		cout<<"Masukkan Lebar Board (harus ganjil) : "; cin>>LebarBoard; //lebar board bingo
		if (LebarBoard % 2 != 0)
			IsValid = true;
	}
	
	IsValid = false;
	while (!IsValid)
	{
		cout<<"Masukkan Range Nilai yang diperbolehkan : "; cin>>RangeNilai; //range nilai yang diperbolehkan
		if (LebarBoard * LebarBoard <= RangeNilai)
			IsValid = true;
	}
	
	ClientSocket connect_socket (IPTracker,5051);
	connect_socket << Name; //memberikan Nama
	connect_socket >> reply; //menunggu reply dari server
	
	cout<<reply<<endl; //menuliskan reply dari server
	connect_socket << "Server"; // Jenis program, apakah server atau client
	connect_socket << IntToString(SPort); //port program
	connect_socket >> my_IP;
	cout<<"IP Address anda : "<<my_IP<<endl;
	//Server membutuhkan 3 data yang dikirim untuk keluar dari tracker
	
	ServerSocket new_sock;
	ServerSocket new_sock_prev;
	
	cout << "Listening @ "<<SPort<<endl;
	cout << "Menunggu lawan" <<endl;
	try
	{
		ServerSocket server(SPort); //connect ke next
		sockaddr_in sockaddress;
		server.accept(new_sock); //menerima message
  		sockaddress = server.GetSockAddress(); //Get sockaddr dari client
  		
  		new_sock << my_IP; //mengirim data
  		new_sock >> reply; //data dummy
  		new_sock << IntToString(SPort+JmlOrang); //mengirim data
  		
  		string IPNext = inet_ntoa(sockaddress.sin_addr); //Next IP Address ini adalah IP client yang konek kesini
  		PortNext = SPort; //portNext ini adalah tempat dia ngebind ini.
  		cout<<SPort+JmlOrang<<endl;
  		
  		ServerSocket server_prev(SPort+JmlOrang); //connect to prev
  		cout<<"Listening Last Peer @ "<<SPort+JmlOrang<<endl;
  		server_prev.accept(new_sock_prev); //menerima last peer
  		connect_socket << "QUIT"; //flags buat quit dari tracker
  		
  		/* Main Algoritma ada disini */
  		Initialize(B);
  		ClearScreen();
  		
  		new_sock << IntToString(LebarBoard); //mengirimkan lebar board kepada client
	  	new_sock >> data;	
	  	new_sock << IntToString(RangeNilai); //mengirimkan batasan nilai kepada client
		
		new_sock_prev >> data;
		new_sock_prev << "OKE";
		new_sock_prev >> data;
		
		FasaMulai(B); //pasang board
		new_sock << "YOU";
		new_sock >> reply;
		
		cout<<"Menunggu lawan. . ."<<endl;
		new_sock_prev >> reply;
		new_sock_prev << "OK";
		//End of pasang board
		
		while (!IsDone)
		{
			ClearScreen();
			PrintBingo(B);
			
			if (isGiliran)
			{
				cout<<"Nilai yang anda minta : "; cin>>TebakanSaya;
				if (SearchNumberLocation(B,TebakanSaya,col,row)) //mencari nomor tebakan sendiri pada board 
				{
					B.a[row][col] = Marked;
				}
			
				new_sock << IntToString(TebakanSaya);
				new_sock >> reply;
				new_sock << "YOU";
				new_sock >> reply;
				if (CountXAll(B) >= Menang) //jika menang
				{
					ClearScreen();
					cout<<"Selamat, anda memenangkan pertandingan "<<endl;
					new_sock << Name;
					cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
					sleep(3);
					IsDone = true;
				}
				else
				{
					new_sock << "0";
					new_sock_prev >> reply;
					new_sock_prev << "OK";
					new_sock_prev >> reply;
					new_sock_prev << "OK";
					new_sock_prev >> status; //penentuan status menang
					if (status != "0")
					{
						ClearScreen();
						cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
						cout<<"Pemenangnya adalah "<<status<<endl;
						cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
						sleep(3);
						IsDone = true;
					}
				}
				
				isGiliran = false;
			}
			else
			{
				cout<<"Menunggu pilihan lawan . . ."<<endl;
				new_sock_prev >> reply;
				TebakanLawan = StringToInt(reply);
				if (SearchNumberLocation(B,TebakanLawan,col,row))
				//mencari nomor tebakan lawan pada bingo
				{
					B.a[row][col] = Marked;
				}	
				new_sock_prev << "OK";
				new_sock_prev >> reply;
				new_sock_prev << "OK";
				new_sock_prev >> status; //penentuan menang / kalah
				
				if (status != "0")
				{
					ClearScreen();
					cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
					cout<<"Pemenangnya adalah "<<status<<endl;
					cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
					IsDone = true;
				}
							
				
				if (reply == "YOU")
					isGiliran = true;
				
				new_sock << IntToString(TebakanLawan);
				new_sock >> reply;
				new_sock << "Not";
				new_sock >> reply;
				if (status != "0")
				{
					new_sock << status;
					sleep(3);
				}
				else
				{
					new_sock << "0";
				}
			}
			
		}
		
		cout<<"Sukses"<<endl;
		
		/* End of Main Algoritma */
	}
	catch (SocketException& e)
	{
		cout<<"Program telah menangkap eksepsi : "<<e.description()<<"\n";
	}
	return 0;
}
