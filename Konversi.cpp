#include "Konversi.h"

using namespace std;

void StringToChar(string a, char b[100])
{
	int i;
	for (i=0;i<a.length();i++)
	{
		b[i] = a[i];
	}
	b[i] = '\0';
}

int CharToInt(char CC)
{
	if (CC == '1')
		return 1;
	else if (CC == '2')
		return 2;
	else if (CC == '3')
		return 3;
	else if (CC == '4')
		return 4;
	else if (CC == '5')
		return 5;
	else if (CC == '6')
		return 6;
	else if (CC == '7')
		return 7;
	else if (CC == '8')
		return 8;
	else if (CC == '9')
		return 9;
	else if (CC == '0')
		return 0;
}

string IntToChar(int CC) //string tapi satu karakter
{
	if (CC == 1)
		return "1";
	else if (CC == 2)
		return "2";
	else if (CC == 3)
		return "3";
	else if (CC == 4)
		return "4";
	else if (CC == 5)
		return "5";
	else if (CC == 6)
		return "6";
	else if (CC == 7)
		return "7";
	else if (CC == 8)
		return "8";
	else if (CC == 9)
		return "9";
	else if (CC == 0)
		return "0";
}

int StringToInt(string S)
{
	int idx;
	idx = 0;
	
	int hasil = 0;
	
	while (idx < S.length())
	{
		hasil = (S[idx] - '0') + hasil * 10;
		idx++;
	}
	return hasil;
}

string IntToString(int I)
{
	if (I % 10 == I)
	{
		return IntToChar(I);
	}
	else
	{
		return (IntToString(I/10) + IntToChar(I%10));
	}
}
