#include <iostream>
#include <cstring>
#include <stdlib.h>
#include "listpemain.h"

/* Helper untuk tracker */
/* Ini List Rekursif dengan paradigma Object Oriented */
/* Kegunaan : List semua client yang sedang online */

using namespace std;

List ListPemain::L = NULL;
int ListPemain::NList = 0;

ListPemain::ListPemain()
{
	//Constructor
	NList++;
}


ListPemain::ListPemain(const ListPemain& P)
{
	//Copy Constructor
	List temp;
	temp = P.L;
	address A;
	
	L = NULL;
	
	while (temp != NULL)
	{
		A = Alokasi(temp->Info.ID,temp->Info.IPAddress,temp->Info.Nama,temp->Info.Jenis,temp->Info.Port);
		A->Next = L;
		L = A;
		temp = temp->Next;
	}
	NList++;
}


ListPemain::~ListPemain()
{
	NList--;
}


ListPemain& ListPemain::operator=(const ListPemain& P)
{
	List temp;
	temp = P.L;
	address A;
	
	L = NULL;
	
	while (temp != NULL)
	{
		A = Alokasi(temp->Info.ID,temp->Info.IPAddress,temp->Info.Nama,temp->Info.Jenis,temp->Info.Port);
		A->Next = L;
		L = A;
		temp = temp->Next;
	}
}


Pemain ListPemain::FirstElmt()
{
	//First Element dari List
	return L->Info;
}

List ListPemain::Tail()
{
	//Next Elemen dari List adalah List
	return L->Next;
}

address ListPemain::Alokasi(int ID, char IPAddress[100], string Nama, string Jenis, string Port)
{
	//Alokasi untuk pemain baru
	address A;
	
	Pemain PM;
	PM.ID = ID;
	strcpy(PM.IPAddress,IPAddress);
	PM.Nama = Nama;
	PM.Jenis = Jenis;
	PM.Port = Port;
	
	A = new tElmtList;
	A->Info = PM;
	A->Next = NULL;
	return A;
}

void ListPemain::AddList(int ID, char IPAddress[100], string Nama, string Jenis, string Port)
{
	//Ini merupakan "Konso" untuk List Rekursif
	address P;
	P = Alokasi(ID,IPAddress,Nama,Jenis,Port);
	P->Next = L;
	L = P;
}

void ListPemain::DelList(int ID)
{
	//Delete pemain yang ber ID...
	address PrevP,P,NextP;
	
	P = L;
	
	if (P->Info.ID == ID) //hapus depan
	{
		L = P->Next;
		delete P;
	}
	else
	{
		while (P->Info.ID != ID)
		{
			PrevP = P;
			P = P->Next; //Next P
		} //sampai ketemu
		
		NextP = P->Next;
		PrevP->Next = NextP;
		
		delete P;
	}
}

void ListPemain::EditList(int ID,string IP, string port)
{
	//Delete pemain yang ber ID...
	address PrevP,P,NextP;
	char IPA[100];
	P = L;
	
	StringToChar(IP,IPA);
	
	if (P->Info.ID == ID) //hapus depan
	{
		strcpy(P->Info.IPAddress,IPA);
		P->Info.Port = port;
	}
	else
	{
		while (P->Info.ID != ID)
		{
			PrevP = P;
			P = P->Next; //Next P
		} //sampai ketemu
		
		strcpy(P->Info.IPAddress,IPA);
		P->Info.Port = port;
	}
}

void ListPemain::PrintList()
{
	//Print semua member list static (pemain online)
	Pemain PM;
	List Temp;
	ListPemain Iseng;
	
	Temp = L; //List temporari
	char Output[100];
	
	cout<<"========================================================================="<<endl;
	cout<<"ID        IP Asal           Name            Jenis                Port"<<endl;
	cout<<"========================================================================="<<endl;
	if (Temp == NULL)
		cout<<"Tidak ada yang online "<<endl;
	else
	{
		while (Temp != NULL)
		{
			PM = Temp->Info;
			cout<<PM.ID<<"   --    "<<PM.IPAddress<<"   --   "<<PM.Nama<<"    --   "<<PM.Jenis<<"	--	"<<PM.Port<<endl;
			
			Temp = Temp->Next;
		}
	}
	cout<<"========================================================================="<<endl;
}

int ListPemain::GetNList()
{
	//mengambil banyaknya pemain online saat ini
	return NList;
}

bool ListPemain::SearchIP(int ID, char Output[100])
{
	Pemain PM;
	List Temp;
	Temp = L; //List temporari
	if (Temp == NULL)
		return false;
	else
	{
		while (Temp != NULL)
		{
			PM = Temp->Info;
			if (PM.ID == ID)
			{
				strcpy(Output,PM.IPAddress);
				return true;
				break;
			}
			
			Temp = Temp->Next;
		}
		return false; //jika sampai disini belum ketemu
	}
}	

bool ListPemain::SearchPort(int ID, string &Output)
{
	Pemain PM;
	List Temp;
	Temp = L; //List temporari
	if (Temp == NULL)
		return false;
	else
	{
		while (Temp != NULL)
		{
			PM = Temp->Info;
			if (PM.ID == ID)
			{
				Output = PM.Port;
				return true;
				break;
			}
			
			Temp = Temp->Next;
		}
		return false; //jika sampai disini belum ketemu
	}
}

List ListPemain::GetList()
{
	return L;
}
