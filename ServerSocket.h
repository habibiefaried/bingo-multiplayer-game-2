// Definition of the ServerSocket class

#ifndef ServerSocket_class
#define ServerSocket_class

#include "Socket.h"


class ServerSocket : public Socket
{
	public:
		ServerSocket (int port);
		ServerSocket (){};
		virtual ~ServerSocket();

		const ServerSocket& operator << (const infotype&) const; //Operator overload untuk send
		const ServerSocket& operator >> (infotype&) const; //operator overload untuk receive
		void accept (ServerSocket&); //Accept

};


#endif
