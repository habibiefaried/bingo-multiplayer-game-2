#ifndef LISTPEMAIN_H
#define LISTPEMAIN_H
#include <cstring>
#include "Konversi.h"

/* Helper untuk tracker */
/* Ini List Rekursif dengan paradigma Object Oriented */
/* Kegunaan : List semua client yang sedang online */

using namespace std;

typedef struct
{
	int ID;
	char IPAddress[100];
	string Nama;
	string Jenis; //create (Server), join (Client)
	string Port;
}Pemain;

typedef struct tElmtList *address;

typedef struct tElmtList
{
        Pemain Info;
        address Next;
}ElmtList;

typedef address List;

class ListPemain
{
	public:
		ListPemain();
		ListPemain(const ListPemain&);
		~ListPemain();
		ListPemain& operator=(const ListPemain&);
		
		address Alokasi(int,char[100],string,string,string);
		static Pemain FirstElmt();
		static List Tail();
		
		void AddList(int,char[100],string,string,string); //pemain-ke, IP Address, Nama, Port
		void DelList(int);
		void EditList(int,string,string); //yang ganti adalah jenis dan port
		
		static void PrintList();
		static int GetNList();
		
		bool SearchIP(int,char[100]); //array of char variabel output adalah keluaran
		bool SearchPort(int,string&); //array of char variabel output adalah keluaran
		string GiveListToClient(int);
		List GetList();
	private:
		static List L;
		static int NList;
};

#endif
