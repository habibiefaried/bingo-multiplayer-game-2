#include "game.h"
#include "Socket.h"

using namespace std;

extern int RangeNilai;
extern int LebarBoard;

void Initialize(Bingo &B)
//F.S Inisialiasi Board Bingo dengan angka 0
{
	int i,j;
	for (i=0;i<=LebarBoard-1;i++)
	{
		for (j=0;j<=LebarBoard-1;j++)
		{
			B.a[i][j] = 0;
		}
	}
	B.kepunyaan = 1;
	B.IsMenang = 0;
}

void PrintBingo(const Bingo &B)
//F.S Mencetak Board Bingo ke Layar
{
	int i,j;
	
	for (i=0;i<=LebarBoard-1;i++)
	{
		for (j=0;j<=LebarBoard-1;j++)
		{
			if (B.a[i][j] == Marked)
				cout<<"X  ";
			else
				cout<<B.a[i][j]<<"  ";
		}
		cout<<endl;
	}
	//cout<<"Count : "<<CountXAll(B)<<endl;
}

bool IsInputValid(int row, int col, int Nilai)
//Memeriksa apakah input dari user Valid
//I.S input terdefinisi
//F.S mengembalikan nilai TRUE apabila masukan user valid
{
	if ((Nilai >= 1) && (Nilai <= RangeNilai))
	{
		if ((row >= 1) && (row <= LebarBoard))
		{
			if ((col >= 1) && (col <= LebarBoard))
				return true;
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;
}

void IsiBingo(Bingo &B, int row, int col, int Nilai)
//Mengisi Board bingo dengan nilai dan tempat yang dimasukkan oleh user
{
	if (IsInputValid(row,col,Nilai))
		B.a[row-1][col-1] = Nilai;
}

bool IsFull(Bingo &B)
//Mengembalikan Nilai true apabila board bingo sudah terisi Penuh
{
	int i,j;
	for (i=0;i<=LebarBoard-1;i++)
	{
		for (j=0;j<=LebarBoard-1;j++)
		{
			if (B.a[i][j] == 0)
			{
				return false;
				break;
			}
		}
	}
	//jika keluar dari loop, maka selesai
	return true;
}

void FasaMulai(Bingo &B)
//Fasa Mulai : Mengisi board bingo hingga penuh 
{
	int col,row,nilai;
	while (!IsFull(B))
	{
		PrintBingo(B);
		cout<<endl;
		cout<<"******************"<<endl;
		cout<<"Isi Nilai"<<endl;
		cout<<"******************"<<endl;
		cout<<"Baris ke : "; cin>>row;
		cout<<"Kolom ke : "; cin>>col;
		cout<<"Nilai (1.."<<RangeNilai<<") : "; cin>>nilai;
		IsiBingo(B,row,col,nilai);
		ClearScreen();
	}
	PrintBingo(B);
}

bool SearchNumberLocation(const Bingo &B, int Tebak, int &col, int &row)
{
//Keluarannya adalah boolean, col dan row dan mengembalikan nilai TRUE (jika ketemu)
	int i,j;
	for (i=0;i<=LebarBoard-1;i++)
	{
		for (j=0;j<=LebarBoard-1;j++)
		{
			if (B.a[i][j] == Tebak)
			{
				row = i;
				col = j;
				return true;
			}
		}
	}
	
	return false;
}

/* Hitung semua X yang berurutan (horizontal dan vertikal) */
bool XHorizontal(const Bingo &B, int row)
//F.S Mengembalikan TRUE apabila satu kolom itu bertanda X semua
{
	int i;
	for (i=0;i<=LebarBoard-1;i++)
	{
		if (B.a[row][i] != Marked)
		{
			return false;
			break;
		}
	}
	return true;
}

bool XVertikal(const Bingo &B, int col)
//F.S Mengembalikan TRUE apabila satu baris bertanda X semua
{
	int i;
	for (i=0;i<=LebarBoard-1;i++)
	{
		if (B.a[i][col] != Marked)
		{
			return false;
			break;
		}
	}
	return true;
}

bool XDiagonalKanan(const Bingo &B)
{
	int i,j;
	i = 0;
	j = -1;
	for (i=0;i<=LebarBoard-1;i++)
	{
		j++;
		if (B.a[i][j] != Marked)
		{
			return false;
			break;
		}
	}
	return true;
}

bool XDiagonalKiri(const Bingo &B)
{
	int i,j;
	i = 0;
	j = LebarBoard;
	for (i=0;i<=LebarBoard-1;i++)
	{
		j--;
		if (B.a[i][j] != Marked)
		{
			return false;
			break;
		}
	}
	return true;
}

int CountXAll(const Bingo &B)
//F.S Menghitung semua X yang berderet
{
	int Count = 0;
	int i;
	
	for (i=0;i<=LebarBoard-1;i++)
	{
		if (XHorizontal(B,i)) Count++;
		if (XVertikal(B,i))	Count++;
	}
	if (XDiagonalKanan(B)) Count++;
	if (XDiagonalKiri(B)) Count++;
	
	return Count;
}

/* End of Menghitung */
